﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using DAL;
using System.Diagnostics;
using System.Text;
using MailChimp;

namespace BlogsAndEmailManagement
{
    public partial class MainForm : Form
    {
        public string InputFileName { get; set; }
        public bool SkipGetRecord { get; set; }
        public long BlogId { get; set; }
        public string OutputFolder { get; set; }

        private string outputPath = @"C:\Temp";

        private MailChimpManager mc;

        public MainForm()
        {
            InitializeComponent();
            this.lstBlogs.Visible = false;
            this.saveToolStripMenuItem.Visible = false;
            this.exportToolStripMenuItem.Visible = false;
            this.SkipGetRecord = true;
            this.ListBlogsAndEmails();
            this.SkipGetRecord = false;

            this.txtMCFrom.Text = "ZipMart";
            this.txtMCSubject.Text = "Online Course";
            this.Width = this.btnFilter.Left + this.btnFilter.Width + 20;

            this.PopulateMailChimpAPIKey();
        }

        private void PopulateMailChimpAPIKey()
        {
            using (ZET_DATAEntities ze = new ZET_DATAEntities())
            {
                MailChimpAPIKey[] apiKeys = ze.MailChimpAPIKey.OrderBy(x => x.DisplaySequence).ToArray();
                this.ddMailChimpAPIKey.DataSource = apiKeys;
                this.ddMailChimpAPIKey.DisplayMember = "Owner";
                this.ddMailChimpAPIKey.ValueMember = "APIKey";
                //var items = this.ddMailChimpAPIKey.Items;
                //foreach (var r in apiKeys)
                //{
                //    items.Add(r.Owner + " - " + r.APIKey);
                //}
            }
        }


        private void ListBlogsAndEmails()
        {
            Blogs[] templates = StaticClass.GetTemplates();
            this.lstBlogs.ValueMember = "BlogId";
            this.lstBlogs.DisplayMember = "Name";

            this.lstEmails.ValueMember = "BlogId";
            this.lstEmails.DisplayMember = "Name";

            this.lstBlogs.DataSource = templates.Where(x => x.Type == 1).ToList();
            this.lstEmails.DataSource = templates.Where(x => x.Type == 2).ToList();

            this.lstBlogs.ClearSelected();
            this.lstEmails.ClearSelected();
            this.txtFileContents.Text = "";
            this.txtTemplateName.Text = "";
        }

        private void PopulateVendorCheckboxes()
        {
            var items = this.VendorList.Items;
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpVendor[] vendorList = ze.MpVendor.Where(x => x.VendorTypeId != 3).OrderBy(x => x.Name).ToArray();

                foreach (MpVendor r in vendorList)
                {
                    items.Add(string.Format("{0} - {1}", r.VendorId, r.Name));
                }
            }
            this.VendorList.Visible = true;
            this.btnFilter.Visible = true;
            this.txtVendorNameFilter.Visible = true;
            this.lblVendors.Visible = true;
            this.Width = this.btnFilter.Left + this.btnFilter.Width + 20;
        }

        // Menu item Open
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.lblMessage.Text = "";

            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {

                this.lblMessage.Text = "Wait .....";
                this.UseWaitCursor = true;

                this.ReadFileContents();

                this.btnDelete.Visible = false;
                this.btnUpdate.Visible = false;
                this.UseWaitCursor = false;
            }
        }

        private void ReadFileContents()
        {
            this.InputFileName = openFileDialog1.FileName;

            string fileExtension = Path.GetExtension(this.InputFileName);
            string[] allowedExtensions = new string[] { ".htm", ".html", ".txt" };

            if (!allowedExtensions.Contains(fileExtension.ToLower()))
            {
                this.lblMessage.Text = "Allowed file types: " + string.Join(",", allowedExtensions) + ". Your file is " + fileExtension + " not allowed!";
                return;
            }

            this.txtTemplateName.Text = StaticClass.ParseDirectory(this.InputFileName, 2);
            this.txtOutputDirectory.Text = StaticClass.ParseDirectory(this.InputFileName, 1);


            try
            {
                string text = File.ReadAllText(this.InputFileName);
                long size = text.Length;
                this.txtFileContents.Text = text;
                this.saveToolStripMenuItem.Visible = true;
                this.btnSaveBlog.Visible = true;
                this.btnSaveEmail.Visible = true;
                this.lblMessage.Text = "File contents shown, you may update it and / or save it to database.";
            }
            catch (Exception ex)
            {
                this.lblMessage.Text = ex.Message;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lstBlogs_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.SkipGetRecord || this.lstBlogs.SelectedValue == null)
                return;

            if (!string.IsNullOrEmpty(this.txtTemplateName.Text) && this.VendorList.Items.Count == 0)
                this.PopulateVendorCheckboxes();

            this.lstEmails.ClearSelected();
            this.BlogId = (long)this.lstBlogs.SelectedValue;
            this.GetBlogRecord(this.BlogId);
        }

        private void lstEmails_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.SkipGetRecord || this.lstEmails.SelectedValue == null)
                return;

            this.lstBlogs.ClearSelected();
            this.BlogId = (long)this.lstEmails.SelectedValue;
            this.GetBlogRecord(this.BlogId);

            if (!string.IsNullOrEmpty(this.txtTemplateName.Text) && this.VendorList.Items.Count == 0)
                this.PopulateVendorCheckboxes();

        }

        private void GetBlogRecord(long blogId)
        {
            Blogs blog = StaticClass.GetTemplate(blogId);
            this.txtTemplateName.Text = blog.Name;
            this.txtFileContents.Text = blog.Contents;
            this.txtAddComments.Text = blog.Comments;
            this.txtPromotional.Text = blog.PromotionalComments;
            this.txtPromoCode.Text = blog.PromoCode;
            this.txtPromoAddlComnt.Text = blog.PromoCodeComment2;
            this.txtTagLine.Text = blog.TagLine;
            this.txtLogoLine.Text = blog.LogoLine;
            this.btnDelete.Visible = true;
            this.btnUpdate.Visible = true;
            this.btnAdd.Visible = true;
            if (!string.IsNullOrEmpty(this.txtPromoCode.Text))
            {
                using (ZipMartEntities ze = new ZipMartEntities())
                {
                    MpPromotion promo = ze.MpPromotion.SingleOrDefault(x => x.PromotionCode == this.txtPromoCode.Text);
                    if (promo != null)
                    {
                        this.txtDiscountAmt.Text = promo.Discount.ToString();
                        if (promo.EndDate != null)
                            this.txtPromoExpDate.Text = ((DateTime)promo.EndDate).ToString("d");
                        this.txtPromoDBComment.Text = promo.Description;
                    }
                }
            }
        }


        private void ExportToBlog()
        {
            this.OutputFolder = this.txtOutputDirectory.Text + "\\ExportBlog";

            if (!Directory.Exists(this.OutputFolder))
            {
                Directory.CreateDirectory(this.OutputFolder);
            }

            this.ProduceOutput(this.OutputFolder, 1);
        }

        private void ExportToEmail()
        {
            this.OutputFolder = this.txtOutputDirectory.Text + "\\ExportEmailBlast";

            if (!Directory.Exists(this.OutputFolder))
            {
                Directory.CreateDirectory(this.OutputFolder);
            }

            this.ProduceOutput(this.OutputFolder, 2);
        }

        private void btnDirSearch_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.txtOutputDirectory.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void ProduceOutput(string folderName, byte outputType) // 1 for Blogs, 2 for Email Blasts
        {
            this.lblMessage.Text = "";
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                int vendorId;
                MpVendor vendor;
                long bannerImageId;
                long logoImageId;
                MpImage img;

                this.progressBar1.Maximum = this.VendorList.CheckedItems.Count + 1;
                this.progressBar1.Value = 1;
                this.progressBar1.Visible = true;

                if (outputType == 2)
                {
                    string vendorOutput;
                    using (ZET_DATAEntities zd = new ZET_DATAEntities())
                    {
                        foreach (var r in this.VendorList.CheckedItems)
                        {
                            vendorId = int.Parse(((string)r).Split('-')[0].Trim());
                            vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                            bannerImageId = vendor.MpImage.Single(x => x.Description == "Banner").ImageId;
                            img = vendor.MpImage.SingleOrDefault(x => x.Description == "ZipMart Vendor Logo");
                            if (img != null)
                                logoImageId = img.ImageId;
                            else
                                logoImageId = 0;

                            vendorOutput = this.BuildVendorOutput(folderName, vendorId, vendor.Name, bannerImageId, logoImageId, outputType);

                            try
                            {
                                mc.AddTemplate(this.txtTemplateName.Text + "_" + vendorId.ToString(), vendorOutput);
                            }
                            catch (Exception ex)
                            {
                                int milisec = DateTime.Now.Millisecond;
                                try
                                {
                                    mc.AddTemplate(this.txtTemplateName.Text + milisec.ToString() + "_" + vendorId.ToString(), vendorOutput);
                                }
                                catch (Exception ex1)
                                {

                                }
                            }

                            if (this.progressBar1.Value < this.progressBar1.Maximum)
                                this.progressBar1.Value += 1;

                            Thread.Sleep(100);

                            MailChimp.Templates.TemplateListResult mcTemplates = mc.GetTemplates();

                            this.RecordSelectedVendorNCourse(vendorId, mcTemplates.UserTemplates.Last().TemplateID, zd);
                        }

                        zd.SaveChanges();
                    }

                    this.PopulateMailchimpTemplates();
                }
                else
                {
                    foreach (var r in this.VendorList.CheckedItems)
                    {
                        vendorId = int.Parse(((string)r).Split('-')[0].Trim());
                        vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                        bannerImageId = vendor.MpImage.Single(x => x.Description == "Banner").ImageId;
                        img = vendor.MpImage.SingleOrDefault(x => x.Description == "ZipMart Vendor Logo");
                        if (img != null)
                            logoImageId = img.ImageId;
                        else
                            logoImageId = 0;

                        this.BuildVendorOutput(folderName, vendorId, vendor.Name, bannerImageId, logoImageId, outputType);

                        if (this.progressBar1.Value < this.progressBar1.Maximum)
                            this.progressBar1.Value += 1;
                    }
                }
            }
            if (this.lblMessage.Text == "")
            {
                this.lblMessage.Text = "File(s) created. Please check your output folder";
                this.btnOutput.Visible = true;
            }
        }

        private void RecordSelectedVendorNCourse(int vendorId, int MCTemplateId, ZET_DATAEntities zd)
        {
            MailChimpTemplate mt = new MailChimpTemplate()
            {
                BlogId = long.Parse(this.lstEmails.SelectedValue.ToString()),
                VendorId = vendorId,
                SelectedCourse = this.GetTopPickCourseId(),
                EmailAddressFrom = this.txtMCEmail.Text,
                FromName = this.txtMCFrom.Text,
                SubjectLine = this.txtMCSubject.Text,
                MailChimpTemplateId = MCTemplateId,
                CreatedDate = DateTime.Now
            };

            zd.MailChimpTemplate.Add(mt);
        }

        private void PopulateMailchimpTemplates()
        {
            var templates = mc.GetTemplates();
            this.lstMailChimpTemplates.Items.Clear();
            var items = this.lstMailChimpTemplates.Items;
            foreach (var r in templates.UserTemplates)
            {
                items.Add(r.TemplateID + " - " + r.Name);
            }
        }

        private void PopulateMailchimpList()
        {
            var lists = mc.GetLists(null, 0, 100, null, null);
            this.lstMailChimpList.Items.Clear();
            var items = this.lstMailChimpList.Items;
            foreach (var r in lists.Data)
            {
                items.Add(r.Id + " - " + r.Name);
            }
        }

        private void PopulateMailchimpCampaigns()
        {
            var campaigns = mc.GetCampaigns();
            this.lstMailChimpCampaigns.Items.Clear();
            var items = this.lstMailChimpCampaigns.Items;
            foreach (var r in campaigns.Data)
            {
                if (r.Status == "sent")
                    items.Add(r.Id + " - " + r.Title + " *Sent*");
                else
                    items.Add(r.Id + " - " + r.Title);
            }
        }


        private string BuildVendorOutput(string folderName, int vendorId, string vendorName, long bannerImageId, long logoImageId, byte outputType)
        {
            string imageShortUrl = "";
            string fbText = "";
            string twitterText = "";
            string courseDescription = "";
            string courseName = "";
            string promotion = "";
            int courseId = 0;
            string courseNameHyperlink = this.GetFirstCourseName(vendorId, out courseName, out courseId, out courseDescription, out imageShortUrl);
            string url = this.GetShortUrlToCourse(vendorId, courseId);
            fbText = this.txtTagLine.Text + "\r\n" + this.txtAddComments.Text + "\r\n" + courseName + "\r\n" + courseDescription + "\r\n\r\n" + this.txtPromotional.Text + " " + this.txtPromoCode.Text + " " + this.txtPromoAddlComnt.Text + "\r\n\r\n" + "https://www.zipmart.com/Brand/" + vendorId.ToString() + "/Source/1/Home/Course/" + courseId.ToString() + "/Description" + "\r\n\r\n" + url + "\r\n\r\n" + promotion + "\r\n\r\n";
            twitterText = this.txtTagLine.Text + "\r\n" + courseName + "\r\n" + url + "\r\n" + this.txtPromotional.Text + " " + this.txtPromoCode.Text + " " + this.txtPromoAddlComnt.Text + "\r\n";

            string prefix = outputType == 1 ? "Blog_" : "Email_";
            try
            {
                string contents = this.txtFileContents.Text.Replace("[?id?]", vendorId.ToString());

                contents = contents.Replace("[?name?]", vendorName);

                contents = contents.Replace("[?banner?]", "<img src=\"http://www.zipmart.com/Images/Image?id=" + bannerImageId.ToString() + "\" alt=\"\" />");

                if (logoImageId > 0)
                    contents = contents.Replace("[?logo?]", "<img src=\"http://www.zipmart.com/Images/Image?id=" + logoImageId.ToString() + "\" alt=\"\" />");
                else
                    contents = contents.Replace("[?logo?]", "<img src=\"http://www.zipmart.com/Images/Image?id=112\" alt=\"\" />");


                string[] words = contents.Split(' ');

                for (int i = 0; i < words.Length; i++)
                {
                    if (words[i].ToLower().Contains("href=\"http://"))
                    {
                        string[] segments = words[i].Split('"');
                        for (int j = 0; j < segments.Length; j++)
                        {
                            if (segments[j].Contains("http://"))
                            {
                                segments[j] = StaticClass.GetShortURL(segments[j]);
                                break;
                            }
                        }

                        words[i] = string.Join("\"", segments);
                    }
                    else if (words[i].Contains("[?courses?]"))
                    {

                        words[i] = ""; // this.AddCourses(vendorId); // This is commented out to ignore [?courses?] because it is conflicting with promo code setup for courses

                    }
                    else if (words[i].Contains("[?cimg?]"))
                    {

                        words[i] = imageShortUrl;

                    }
                    else if (words[i].Contains("[?cname?]"))
                    {

                        words[i] = courseNameHyperlink;

                    }
                    else if (words[i].Contains("[?cdescr?]"))
                    {

                        words[i] = courseDescription;

                    }
                    else if (words[i].Contains("[?enroll?]"))
                    {

                        words[i] = this.CreateEnrollButton(vendorId);

                    }
                    else if (words[i].Contains("[?logoLine?]"))
                    {

                        words[i] = this.txtLogoLine.Text;

                    }
                    else if (words[i].Contains("[?tagline?]"))
                    {

                        words[i] = this.txtTagLine.Text;

                    }
                    else if (words[i].Contains("[?additionalComments?]"))
                    {

                        words[i] = this.txtAddComments.Text.Replace("[?id?]", vendorId.ToString());

                    }
                    else if (words[i].Contains("[?promo?]"))
                    {

                        words[i] = "<p style=\"font-weight:bold;font-size:18px;text-align:center\">" + this.txtPromotional.Text.Replace("[?id?]", vendorId.ToString()) + "<span style=\"background-color:#ffff99\">" + this.txtPromoCode.Text + "</span>" + this.txtPromoAddlComnt.Text + "</p>";

                        promotion = this.txtPromotional.Text.Replace("[?id?]", vendorId.ToString()) + " " + this.txtPromoCode.Text + " " + this.txtPromoAddlComnt.Text;
                    }
                    else if (words[i].Contains("[?fb?]"))
                    {
                        if (this.chkIncludeFBTwitter.Checked && !string.IsNullOrEmpty(fbText) && !string.IsNullOrEmpty(twitterText) && this.lstTopPickCourse.SelectedIndex >= 0)
                            words[i] = "<p style=\"font-size:10px;font-family:courier;color:#707070;background-color:#e0e0e0;padding:10px;\"><b>Copy and Paste to facebook</b>:<br />--------Start------------<br />" + fbText.Replace("\r\n", "<br />") + "<br />---------End-----------<br /><b>Copy and Paste to Twitter / Linked In:</b><br />---------Start-----------<br />" + twitterText.Replace("\r\n", "<br />") + "<br />---------End-----------</p>";
                        else
                            words[i] = "";
                    }

                }

                contents = string.Join(" ", words);

                string filename = vendorName + "_" + vendorId.ToString() + "_" + this.txtTemplateName.Text + "_" + this.txtExportName.Text.Replace(":", "").Replace("/", "").Replace("&", "");

                File.WriteAllText(folderName + "\\" + filename + "_html.htm", contents);

                File.WriteAllText(folderName + "\\" + filename + "_fb.txt", fbText);
                File.WriteAllText(folderName + "\\" + filename + "_twit.txt", twitterText);

                if (!string.IsNullOrEmpty(this.txtPromoCode.Text))
                {
                    this.AddPromoCode(vendorId, courseId);
                }

                return contents;
            }
            catch (Exception ex)
            {
                this.lblMessage.Text = ex.Message;
                return "";
            }
        }

        private void AddPromoCode(int vendorId, int courseId)
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpCourse course = ze.MpCourse.Single(x => x.CourseId == courseId);
                MpVendor vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                MpPromotion promo = ze.MpPromotion.Where(x => x.PromotionCode == this.txtPromoCode.Text).SingleOrDefault();
                if (promo == null)
                {
                    promo = new MpPromotion() { PromotionCode = this.txtPromoCode.Text, Description = "Entered From Email Blast Software", Discount = 0, EndDate = DateTime.Now.AddDays(30) };
                    promo.MpCourse.Add(course);
                    promo.MpVendor.Add(vendor);
                    ze.MpPromotion.Add(promo);
                    ze.SaveChanges();
                }
            }

        }

        private string CreateEnrollButton(int vendorId)
        {
            if (this.lstTopPickCourse.SelectedIndex < 0)
            {
                this.lblMessage.Text = "Please select the main course where the enroll button will go to";
                return "";
            }
            else
            {
                using (ZipMartEntities ze = new ZipMartEntities())
                {
                    MpVendor vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                    int imageId;
                    int courseId = this.GetTopPickCourseId();
                    MpCourseVendor courseVendor = vendor.MpCourseVendor.SingleOrDefault(x => x.CourseId == courseId);
                    if (courseVendor.MpCourse.MpImage != null)
                    {
                        imageId = courseVendor.MpCourse.MpImage.ImageId;
                        return this.Hyperlink(vendorId, courseId);
                    }
                    else
                        return "";
                }
            }
        }

        private string GetShortUrlToCourse(int vendorId, int courseId)
        {
            return StaticClass.GetShortURL("https://www.zipmart.com/Brand/" + vendorId.ToString() + "/Source/1/Home/Course/" + courseId + "/Description?PromoCode=" + this.txtPromoCode.Text);
        }

        private string GetFirstCourseName(int vendorId, out string courseName, out int courseId, out string courseDescr, out string imageShortUrl)
        {
            courseId = 0;
            courseName = "";
            courseDescr = "";
            imageShortUrl = "";
            if (this.lstTopPickCourse.SelectedIndex < 0)
                return "";
            else
            {
                using (ZipMartEntities ze = new ZipMartEntities())
                {
                    MpVendor vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                    courseId = this.GetTopPickCourseId();
                    int cid = courseId;
                    MpCourseVendor courseVendor = vendor.MpCourseVendor.SingleOrDefault(x => x.CourseId == cid);
                    if (courseVendor != null)
                    {
                        courseName = courseVendor.MpCourse.Name;
                        courseDescr = courseVendor.MpCourse.Description;
                        if (courseVendor.MpCourse.MpImage != null)
                        {
                            int imageId = courseVendor.MpCourse.MpImage.ImageId;
                            imageShortUrl = this.Hyperlink(vendorId, courseId, "<img style=\"display:block;width:100%;height:auto;border:none;\" src=\"http://www.zipmart.com/Images/Image?id=" + imageId.ToString() + "\" alt=\"\"/>");
                        }

                        return this.Hyperlink(vendorId, courseId, courseName);
                    }
                    else
                        return "";
                }
            }
        }

        private int GetTopPickCourseId()
        {
            return int.Parse(this.lstTopPickCourse.Items[this.lstTopPickCourse.SelectedIndex].ToString().Split(' ')[0].Trim());
        }

        private int[] GetSelectedCourseIds()
        {
            List<int> courseIds = new List<int>();
            foreach (var r in this.CourseList.CheckedItems)
            {
                courseIds.Add(int.Parse(((string)r).Split(' ')[0].Trim()));
            }

            return courseIds.ToArray();
        }

        private string AddCourses(int vendorId)
        {
            StringBuilder sb = new StringBuilder();
            int courseId;
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpVendor vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                int imageId;
                sb.AppendLine("<div>");
                foreach (var c in CourseList.CheckedItems)
                {
                    courseId = int.Parse(((string)c).Split(' ')[0].Trim());
                    MpCourseVendor courseVendor = vendor.MpCourseVendor.SingleOrDefault(x => x.CourseId == courseId);
                    if (courseVendor != null)
                    {
                        MpCourse course = courseVendor.MpCourse;
                        if (course.MpImage != null)
                        {
                            imageId = course.MpImage.ImageId;
                            sb.AppendLine("<div class=\"row\"><div class=\"col-sm-8\">");
                            sb.Append(this.Hyperlink(vendorId, courseId, "<img style=\"display:block;width:100%;height:auto;border:none;\" src=\"http://www.zipmart.com/Images/Image?id=" + imageId.ToString() + "\" alt=\"\"/>"));
                            sb.AppendLine("</div>\r\n<div class=\"col-sm-12 text-justify\">");
                            sb.Append(this.Hyperlink(vendorId, courseId, course.Name));
                            sb.AppendLine("<br />" + course.Description);
                            sb.AppendLine("</div></div>\r\n");
                        }
                    }
                }
                sb.AppendLine("</div>");
            }
            return sb.ToString();
        }

        private string Hyperlink(int vendorId, int courseId, string contents)
        {
            return "<a style=\"text-decoration:none\" href=\"" + StaticClass.GetShortURL("https://www.zipmart.com/Brand/" + vendorId.ToString() + "/Source/1/Home/Course/" + courseId.ToString() + "/Description?PromoCode=" + this.txtPromoCode.Text) + "\">" +
            "<b>" + contents + "</b></a> ";
        }

        private string Hyperlink(int vendorId, int courseId)
        {
            string url = "href=\"" + StaticClass.GetShortURL("https://www.zipmart.com/Brand/" + vendorId.ToString() + "/Source/1/Home/Course/" + courseId.ToString() + "/Description?PromoCode=" + this.txtPromoCode.Text) + "\"";
            return url;
        }


        private void txtOutputDirectory_TextChanged(object sender, EventArgs e)
        {
            this.ShowHideExportButtons();
        }

        private void ShowHideExportButtons()
        {
            if (!string.IsNullOrEmpty(txtOutputDirectory.Text))
            {
                if (this.VendorList.CheckedItems.Count > 0)
                {
                    if (!string.IsNullOrEmpty(this.txtPromoCode.Text))
                        this.btnAddPromoCode.Visible = true;
                    else
                        this.btnAddPromoCode.Visible = false;

                    if (!string.IsNullOrEmpty(this.txtFileContents.Text))
                    {
                        this.exportToolStripMenuItem.Visible = true;
                        this.CourseList.Visible = true;
                        this.lstTopPickCourse.Visible = true;
                        this.Width = this.CourseList.Left + this.CourseList.Width + 50;
                        this.PopulateCourseList(true);
                        this.label1.Visible = true;
                        this.label3.Visible = true;
                        this.txtSearch.Visible = true;
                    }
                    else
                    {
                        this.exportToolStripMenuItem.Visible = false;
                        this.CourseList.Visible = false;
                        this.lstTopPickCourse.Visible = false;
                        this.Width = this.btnFilter.Left + this.btnFilter.Width + 30;
                        this.label1.Visible = false;
                        this.label3.Visible = false;
                        this.txtSearch.Visible = false;
                    }
                }
                else
                {
                    this.exportToolStripMenuItem.Visible = false;
                    this.CourseList.Visible = false;
                    this.lstTopPickCourse.Visible = false;
                    this.Width = this.btnFilter.Left + this.btnFilter.Width + 30;
                    this.label1.Visible = false;
                    this.label3.Visible = false;
                    this.txtSearch.Visible = false;
                }
            }
            else
            {
                this.exportToolStripMenuItem.Visible = false;
                this.CourseList.Visible = false;
                this.lstTopPickCourse.Visible = false;
                this.Width = this.btnFilter.Left + this.btnFilter.Width + 30;
                this.label1.Visible = false;
                this.label3.Visible = false;
                this.txtSearch.Visible = false;
            }
        }

        private void PopulateCourseList(bool populatemultiCourseSelection)
        {
            this.chkIncludeFBTwitter.Visible = false;
            this.chkIncludeFBTwitter.Checked = false;

            List<int> selectedVendors = new List<int>();
            foreach (var r in this.VendorList.CheckedItems)
            {
                selectedVendors.Add(int.Parse(((string)r).Split('-')[0].Trim()));
            }

            CheckedListBox.ObjectCollection items = null;
            if (populatemultiCourseSelection)
            {
                this.CourseList.Items.Clear();
                items = this.CourseList.Items;
            }

            this.lstTopPickCourse.Items.Clear();

            this.btnExportBlog.Visible = false;
            this.btnExportEmail.Visible = false;
            this.btnPreview.Visible = false;
            this.chkIncludeFBTwitter.Visible = false;
            this.chkIncludeFBTwitter.Checked = false;

            var items1 = this.lstTopPickCourse.Items;

            using (ZipMartEntities ze = new ZipMartEntities())
            {
                int[] courseVendors = ze.MpCourseVendor.Where(x => selectedVendors.Contains(x.VendorId)).Select(y => y.CourseId).Distinct().ToArray();
                MpCourse[] courses;
                if (this.rd2.Checked)
                    courses = ze.MpCourse.Where(x => courseVendors.Contains(x.CourseId) && (string.IsNullOrEmpty(this.txtSearch.Text) || x.Name.Contains(this.txtSearch.Text))).OrderBy(y => y.CourseId).ToArray();
                else
                    courses = ze.MpCourse.Where(x => courseVendors.Contains(x.CourseId) && (string.IsNullOrEmpty(this.txtSearch.Text) || x.Name.Contains(this.txtSearch.Text))).OrderBy(y => y.Name).ToArray();

                foreach (MpCourse c in courses)
                {
                    items1.Add(string.Format("{0} - {1}", c.CourseId, c.Name));
                    if (populatemultiCourseSelection)
                        items.Add(string.Format("{0} - {1}", c.CourseId, c.Name));
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure you want to delete this record?", "Confirm Delete", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                this.lblMessage.Text = StaticClass.DeleteTemplate(this.BlogId);
                this.ListBlogsAndEmails();
                this.txtFileContents.Text = "";
                this.txtTemplateName.Text = "";
                this.BlogId = 0;
                this.btnDelete.Visible = false;
                this.btnUpdate.Visible = false;
                this.btnSaveEmail.Visible = false;
                this.btnSaveBlog.Visible = false;
            }
        }

        private void VendorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowHideExportButtons();
            this.chkCheckAll.Visible = true;
        }

        private void btnOutput_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.OutputFolder))
            {
                this.lblMessage.Text = "Please define your output directory";
                return;
            }
            Process.Start(this.OutputFolder);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure you want to update this record?", "Confirm Update", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                this.lblMessage.Text = StaticClass.UpdateTemplate(this.BlogId, this.txtTemplateName.Text, this.txtFileContents.Text, this.txtLogoLine.Text, this.txtTagLine.Text, this.txtAddComments.Text, this.txtPromotional.Text, this.txtPromoCode.Text, this.txtPromoAddlComnt.Text);
                this.ListBlogsAndEmails();
            }

        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            this.VendorList.Items.Clear();

            var items = this.VendorList.Items;
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                MpVendor[] vendorList = null;
                if (!string.IsNullOrEmpty(this.txtVendorNameFilter.Text))
                    vendorList = ze.MpVendor.Where(x => x.VendorTypeId != 3 && x.Name.Contains(this.txtVendorNameFilter.Text)).OrderBy(x => x.Name).ToArray();
                else
                    vendorList = ze.MpVendor.Where(x => x.VendorTypeId != 3).OrderBy(x => x.Name).ToArray();

                foreach (var r in vendorList)
                {
                    items.Add(string.Format("{0} - {1}", r.VendorId, r.Name));
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!Directory.Exists(outputPath))
                Directory.CreateDirectory(outputPath);

            txtOutputDirectory.Text = folderBrowserDialog1.SelectedPath = outputPath;

            this.Width = this.btnDeleteCampaign.Left + this.btnDeleteCampaign.Width + 30;
        }

        private void aboutThisSoftwareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help help = new Help();
            help.Show();
        }

        private void SaveToEmail()
        {
            this.lblMessage.Text = StaticClass.StoreTemplate(this.txtTemplateName.Text, this.txtFileContents.Text, 2, this.txtLogoLine.Text, this.txtTagLine.Text, this.txtAddComments.Text, this.txtPromotional.Text, this.txtPromoCode.Text, this.txtPromoAddlComnt.Text);
            this.RepopulateBlogsAndEmailDropdown();
        }

        private void SaveToBlog()
        {
            this.lblMessage.Text = StaticClass.StoreTemplate(this.txtTemplateName.Text, this.txtFileContents.Text, 1, this.txtLogoLine.Text, this.txtTagLine.Text, this.txtAddComments.Text, this.txtPromotional.Text, this.txtPromoCode.Text, this.txtPromoAddlComnt.Text);
            this.RepopulateBlogsAndEmailDropdown();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.lblMessage.Text = StaticClass.CreateTemplate(this.txtTemplateName.Text, this.txtFileContents.Text, this.txtLogoLine.Text, this.txtTagLine.Text, this.txtAddComments.Text, 2, this.txtPromotional.Text, this.txtPromoCode.Text, this.txtPromoAddlComnt.Text);
            this.RepopulateBlogsAndEmailDropdown();
        }

        private void RepopulateBlogsAndEmailDropdown()
        {
            this.SkipGetRecord = true;
            this.ListBlogsAndEmails();
            this.SkipGetRecord = false;
        }


        private void blogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SaveToBlog();
        }

        private void emailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SaveToEmail();
        }

        private void btnSaveBlog_Click(object sender, EventArgs e)
        {
            this.SaveToBlog();
        }

        private void btnSaveEmail_Click(object sender, EventArgs e)
        {
            this.SaveToEmail();
        }

        private void btnExportEmail_Click(object sender, EventArgs e)
        {
            this.ExportToEmail();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            this.ExportToBlog();
        }

        private void toBlogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ExportToBlog();
        }

        private void toEmailBlastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ExportToEmail();
        }

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkCheckAll.Checked)
            {
                for (int i = 0; i < this.VendorList.Items.Count; i++)
                    this.VendorList.SetItemChecked(i, true);
            }
            else
            {
                for (int i = 0; i < this.VendorList.Items.Count; i++)
                    this.VendorList.SetItemChecked(i, false);
            }
        }

        private void btnEmailBlast_Click(object sender, EventArgs e)
        {
            if (this.lstMailChimpCampaigns.SelectedIndex < 0)
            {
                this.lblMessage.Text = "Please select a campaign to send the email blast";
                return;
            }
            else
            {
                var dialogResult = MessageBox.Show("This campaign can only be sent once. Are you sure you want to send this campaign?", "Please Confirm", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    string campaignId = this.lstMailChimpCampaigns.Items[this.lstMailChimpCampaigns.SelectedIndex].ToString().Split(' ')[0];
                    var result = mc.SendCampaign(campaignId);
                    if (result.Complete)
                    {
                        this.lblMessage.Text = "Email Blast has been sent";
                        this.lstMailChimpCampaigns.SelectedIndex = -1;
                        this.btnEmailBlast.Visible = false;
                        this.PopulateMailchimpCampaigns();
                    }
                }
            }
        }

        private string[] GetVendorNames()
        {
            using (ZipMartEntities ze = new ZipMartEntities())
            {
                List<string> vendorNames = new List<string>();
                int vendorId = 0;
                foreach (var r in this.VendorList.CheckedItems)
                {
                    vendorId = int.Parse(r.ToString().Split(' ')[0]);
                    vendorNames.Add(ze.MpVendor.Single(x => x.VendorId == vendorId).Name);
                }
                return vendorNames.ToArray();
            }
        }

        private void btnCreateCampaign_Click(object sender, EventArgs e)
        {
            if (this.lstMailChimpTemplates.SelectedIndex < 0 || this.lstMailChimpList.SelectedIndex < 0)
            {
                this.lblMessage.Text = "Please select a template and list, to create campaign";
                return;
            }
            else
            {
                int templateId = int.Parse(this.lstMailChimpTemplates.Items[this.lstMailChimpTemplates.SelectedIndex].ToString().Split(' ')[0]);
                string listId = this.lstMailChimpList.Items[this.lstMailChimpList.SelectedIndex].ToString().Split(' ')[0];
                MailChimp.Campaigns.CampaignCreateOptions options = new MailChimp.Campaigns.CampaignCreateOptions()
                {
                    Title = "Camp " + templateId.ToString() + " " + listId,
                    TemplateID = templateId,
                    ListId = listId,
                    Subject = this.txtMCSubject.Text,
                    FromName = this.txtMCFrom.Text,
                    FromEmail = this.txtMCEmail.Text,
                    ToName = "Current Subscribers",
                    FacebookComments = true
                };

                MailChimp.Campaigns.CampaignCreateContent contents = new MailChimp.Campaigns.CampaignCreateContent();
                mc.CreateCampaign("regular", options, contents);
                StaticClass.UpdateMailChimpRecord(templateId, this.txtMCEmail.Text, this.txtMCFrom.Text, this.txtMCSubject.Text);
                this.PopulateMailchimpCampaigns();

            }

        }

        private void lstMailChimpCampaigns_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.txtFileContents.Height = 512;
            this.btnEmailBlast.Visible = true;
        }

        private void lstMailChimpTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtSearch.Text = string.Empty;
            if (this.lstMailChimpTemplates.SelectedIndex >= 0 && this.lstMailChimpList.SelectedIndex >= 0)
            {
                this.btnCreateCampaign.Visible = true;
            }
            else if (this.lstMailChimpTemplates.SelectedIndex >= 0)
            {
                int mctId = int.Parse(this.lstMailChimpTemplates.Items[this.lstMailChimpTemplates.SelectedIndex].ToString().Split(' ')[0]);
                using (ZET_DATAEntities zd = new ZET_DATAEntities())
                {
                    MailChimpTemplate mt = zd.MailChimpTemplate.SingleOrDefault(x => x.MailChimpTemplateId == mctId);
                    if (mt != null)
                    {
                        this.txtMCEmail.Text = mt.EmailAddressFrom;
                        this.txtMCFrom.Text = mt.FromName;
                        this.txtMCSubject.Text = mt.SubjectLine;

                        this.lstBlogs.ClearSelected();
                        this.lstEmails.ClearSelected();

                        this.BlogId = mt.BlogId;
                        this.lstEmails.SelectedValue = mt.BlogId;
                        this.GetBlogRecord(this.BlogId);

                        this.VendorList.ClearSelected();
                        for (int i = 0; i < this.VendorList.Items.Count; i++)
                        {
                            if (int.Parse(this.VendorList.Items[i].ToString().Split(' ')[0]) == mt.VendorId)
                            {
                                this.VendorList.SetItemChecked(i, true);
                                this.VendorList.SelectedIndex = i;
                                this.ShowHideExportButtons();
                                this.chkCheckAll.Visible = true;
                            }
                            else
                                this.VendorList.SetItemChecked(i, false);
                        }

                        for (int i = 0; i < lstTopPickCourse.Items.Count; i++)
                        {
                            if (int.Parse(this.lstTopPickCourse.Items[i].ToString().Split(' ')[0]) == mt.SelectedCourse)
                            {
                                this.lstTopPickCourse.SetSelected(i, true);
                                this.lstTopPickCourse.SelectedIndex = i;
                                break;
                            }
                        }

                    }
                }
            }
        }

        private void lstMailChimpList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lstMailChimpTemplates.SelectedIndex >= 0 && this.lstMailChimpList.SelectedIndex >= 0)
            {
                this.btnCreateCampaign.Visible = true;
            }
            MailChimp.Lists.ListFilter filter = new MailChimp.Lists.ListFilter();
            filter.ListId = this.lstMailChimpList.Items[this.lstMailChimpList.SelectedIndex].ToString().Split(' ')[0];
            var result = mc.GetLists(filter);
            this.txtMCEmail.Text = result.Data[0].DefaultFromEmail;

            if (!string.IsNullOrEmpty(result.Data[0].DefaultFromName))
                this.txtMCFrom.Text = result.Data[0].DefaultFromName;
            else if (this.VendorList.CheckedItems.Count > 0)
                this.txtMCFrom.Text = this.GetVendorNames()[0];
            else
                this.txtMCFrom.Text = "ZipMart";

            if (!string.IsNullOrEmpty(result.Data[0].DefaultSubject))
                this.txtMCSubject.Text = result.Data[0].DefaultSubject;
            else if (!string.IsNullOrEmpty(this.txtTagLine.Text) && !this.txtTagLine.Text.Contains("<"))
                this.txtMCSubject.Text = this.txtTagLine.Text;

        }

        private void btnDeleteCampaign_Click(object sender, EventArgs e)
        {
            if (this.lstMailChimpCampaigns.SelectedIndex < 0)
            {
                this.lblMessage.Text = "Please select a campaign to delete";
                return;
            }
            else
            {
                string campaignId = this.lstMailChimpCampaigns.Items[this.lstMailChimpCampaigns.SelectedIndex].ToString().Split(' ')[0];
                mc.DeleteCampaign(campaignId);
                this.PopulateMailchimpCampaigns();
                this.btnEmailBlast.Visible = false;
            }
        }

        private void btnDeleteTemplate_Click(object sender, EventArgs e)
        {
            if (this.lstMailChimpTemplates.SelectedIndex < 0)
            {
                this.lblMessage.Text = "Please select a template to delete";
                return;
            }
            else
            {
                int templateId = int.Parse(this.lstMailChimpTemplates.Items[this.lstMailChimpTemplates.SelectedIndex].ToString().Split(' ')[0]);
                mc.DeleteTemplate(templateId);
                this.PopulateMailchimpTemplates();
                StaticClass.DeleteMailChimpRecord(templateId);
            }
        }

        private void lstTopPickCourse_Click(object sender, EventArgs e)
        {
            string[] words = this.lstTopPickCourse.Items[this.lstTopPickCourse.SelectedIndex].ToString().Split(' ');

            if (words.Length > 3)
                this.txtExportName.Text = words[2] + "_" + words[3];
            else
                this.txtExportName.Text = words[2];

            this.txtExportName.Visible = true;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.PopulateCourseList(false);
        }

        private void ddMailChimpAPIKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddMailChimpAPIKey.SelectedValue.GetType() == typeof(MailChimpAPIKey))
            {
                MailChimpAPIKey sel = (MailChimpAPIKey)this.ddMailChimpAPIKey.SelectedValue;
                this.mc = new MailChimpManager(sel.APIKey);
                this.PopulateMailChimpLists();
            }
            else if (this.ddMailChimpAPIKey.SelectedValue.GetType() == typeof(string))
            {
                string apiKey = this.ddMailChimpAPIKey.SelectedValue.ToString();
                this.mc = new MailChimpManager(apiKey);
                this.PopulateMailChimpLists();
            }
        }

        private void PopulateMailChimpLists()
        {
            this.PopulateMailchimpTemplates();
            this.PopulateMailchimpCampaigns();
            this.PopulateMailchimpList();
        }

        private void lstTopPickCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnExportBlog.Visible = true;
            this.btnExportEmail.Visible = true;
            this.btnPreview.Visible = true;
            this.chkIncludeFBTwitter.Visible = true;
        }

        private void lstBlogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtTemplateName.Text) && this.VendorList.Items.Count == 0)
                this.PopulateVendorCheckboxes();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            this.progressBar1.Visible = true;
            this.progressBar1.Maximum = this.VendorList.CheckedItems.Count + 1;
            this.progressBar1.Value = 1;

            this.OutputFolder = this.txtOutputDirectory.Text + "\\Preview";

            if (!Directory.Exists(this.OutputFolder))
            {
                Directory.CreateDirectory(this.OutputFolder);
            }

            this.ProduceOutput(this.OutputFolder, 1);
            Process.Start(this.OutputFolder);
        }

        private void btnAddPromoCode_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtPromoCode.Text) && this.VendorList.CheckedItems.Count > 0)
            {
                using (ZipMartEntities ze = new ZipMartEntities())
                {
                    int vendorId;
                    int[] courseIds = this.GetSelectedCourseIds();
                    MpVendor vendor;
                    MpPromotion promo = ze.MpPromotion.SingleOrDefault(x => x.PromotionCode == this.txtPromoCode.Text);
                    if (promo == null)
                    {
                        DateTime promoExpDate;
                        if (!DateTime.TryParse(this.txtPromoExpDate.Text, out promoExpDate))
                        {
                            promoExpDate = DateTime.Now.AddDays(30);
                        }

                        promo = new MpPromotion() { PromotionCode = this.txtPromoCode.Text, Description = "Frm Email Blast " + this.txtPromoDBComment.Text, Discount = 0, EndDate = promoExpDate };
                        double discount;
                        if (double.TryParse(this.txtDiscountAmt.Text, out discount) && (discount >= 0 && discount <= 1))
                            promo.Discount = discount;

                        foreach (var r in this.VendorList.CheckedItems)
                        {
                            vendorId = int.Parse(((string)r).Split('-')[0].Trim());
                            vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                            promo.MpVendor.Add(vendor);
                        }

                        foreach (int c in courseIds)
                        {
                            MpCourse course = ze.MpCourse.Single(x => x.CourseId == c);
                            promo.MpCourse.Add(course);
                        }

                        ze.MpPromotion.Add(promo);
                        if (ze.SaveChanges() > 0)
                            this.lblMessage.Text = "New Promo Code has been added";
                    }
                    else
                    {
                        double discount;
                        if (double.TryParse(this.txtDiscountAmt.Text, out discount) && (discount >= 0 && discount <= 1))
                            promo.Discount = discount;

                        this.lblMessage.Text = "";

                        foreach (int c in courseIds)
                        {
                            if (promo.MpCourse.SingleOrDefault(x => x.CourseId == c) == null)
                            {
                                MpCourse course = ze.MpCourse.Single(x => x.CourseId == c);
                                promo.MpCourse.Add(course);
                            }
                        }

                        foreach (var r in this.VendorList.CheckedItems)
                        {
                            vendorId = int.Parse(((string)r).Split('-')[0].Trim());

                            if (promo.MpVendor.SingleOrDefault(x => x.VendorId == vendorId) == null)
                            {
                                vendor = ze.MpVendor.Single(x => x.VendorId == vendorId);
                                promo.MpVendor.Add(vendor);
                            }
                        }

                        if (ze.SaveChanges() > 0)
                            this.lblMessage.Text = "Vendor/Course Promo updated";

                        if (this.lblMessage.Text == "")
                            this.lblMessage.Text = "Promo code already exists. Please check the discount";
                    }
                }
            }
        }

        private void rd2_CheckedChanged(object sender, EventArgs e)
        {
            this.ShowHideExportButtons();
        }

        private void rd1_CheckedChanged(object sender, EventArgs e)
        {
            this.ShowHideExportButtons();
        }


    }
}
