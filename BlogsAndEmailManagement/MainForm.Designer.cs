﻿namespace BlogsAndEmailManagement
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toBlogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toEmailBlastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutThisSoftwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtFileContents = new System.Windows.Forms.TextBox();
            this.VendorList = new System.Windows.Forms.CheckedListBox();
            this.lblEmails = new System.Windows.Forms.Label();
            this.btnExportBlog = new System.Windows.Forms.Button();
            this.txtTemplateName = new System.Windows.Forms.TextBox();
            this.lstBlogs = new System.Windows.Forms.ListBox();
            this.lstEmails = new System.Windows.Forms.ListBox();
            this.lblVendors = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOutputDirectory = new System.Windows.Forms.TextBox();
            this.btnDirSearch = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnOutput = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtVendorNameFilter = new System.Windows.Forms.TextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.btnSaveBlog = new System.Windows.Forms.Button();
            this.btnSaveEmail = new System.Windows.Forms.Button();
            this.btnExportEmail = new System.Windows.Forms.Button();
            this.chkCheckAll = new System.Windows.Forms.CheckBox();
            this.CourseList = new System.Windows.Forms.CheckedListBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lstTopPickCourse = new System.Windows.Forms.ListBox();
            this.btnEmailBlast = new System.Windows.Forms.Button();
            this.lstMailChimpTemplates = new System.Windows.Forms.ListBox();
            this.lstMailChimpList = new System.Windows.Forms.ListBox();
            this.lstMailChimpCampaigns = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCreateCampaign = new System.Windows.Forms.Button();
            this.btnDeleteCampaign = new System.Windows.Forms.Button();
            this.btnDeleteTemplate = new System.Windows.Forms.Button();
            this.txtAddComments = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtPromotional = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtExportName = new System.Windows.Forms.TextBox();
            this.txtTagLine = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPromoCode = new System.Windows.Forms.TextBox();
            this.txtPromoAddlComnt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ddMailChimpAPIKey = new System.Windows.Forms.ComboBox();
            this.txtMCFrom = new System.Windows.Forms.TextBox();
            this.txtMCSubject = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMCEmail = new System.Windows.Forms.TextBox();
            this.txtLogoLine = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.chkIncludeFBTwitter = new System.Windows.Forms.CheckBox();
            this.btnAddPromoCode = new System.Windows.Forms.Button();
            this.txtDiscountAmt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.rd1 = new System.Windows.Forms.RadioButton();
            this.rd2 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPromoExpDate = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPromoDBComment = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.exportToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1268, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blogToolStripMenuItem,
            this.emailToolStripMenuItem});
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.saveToolStripMenuItem.Text = "&Save To";
            this.saveToolStripMenuItem.Visible = false;
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // blogToolStripMenuItem
            // 
            this.blogToolStripMenuItem.Name = "blogToolStripMenuItem";
            this.blogToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.blogToolStripMenuItem.Text = "Blog Template";
            this.blogToolStripMenuItem.Click += new System.EventHandler(this.blogToolStripMenuItem_Click);
            // 
            // emailToolStripMenuItem
            // 
            this.emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            this.emailToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.emailToolStripMenuItem.Text = "Email Template";
            this.emailToolStripMenuItem.Click += new System.EventHandler(this.emailToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toBlogsToolStripMenuItem,
            this.toEmailBlastToolStripMenuItem});
            this.exportToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.exportToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.exportToolStripMenuItem.Text = "&Export";
            this.exportToolStripMenuItem.Visible = false;
            // 
            // toBlogsToolStripMenuItem
            // 
            this.toBlogsToolStripMenuItem.Name = "toBlogsToolStripMenuItem";
            this.toBlogsToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.toBlogsToolStripMenuItem.Text = "To Blogs";
            this.toBlogsToolStripMenuItem.Click += new System.EventHandler(this.toBlogsToolStripMenuItem_Click);
            // 
            // toEmailBlastToolStripMenuItem
            // 
            this.toEmailBlastToolStripMenuItem.Name = "toEmailBlastToolStripMenuItem";
            this.toEmailBlastToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.toEmailBlastToolStripMenuItem.Text = "To Email Blast";
            this.toEmailBlastToolStripMenuItem.Click += new System.EventHandler(this.toEmailBlastToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutThisSoftwareToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutThisSoftwareToolStripMenuItem
            // 
            this.aboutThisSoftwareToolStripMenuItem.Name = "aboutThisSoftwareToolStripMenuItem";
            this.aboutThisSoftwareToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.aboutThisSoftwareToolStripMenuItem.Text = "About This Software";
            this.aboutThisSoftwareToolStripMenuItem.Click += new System.EventHandler(this.aboutThisSoftwareToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.Red;
            this.lblMessage.Location = new System.Drawing.Point(184, 33);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(54, 15);
            this.lblMessage.TabIndex = 1;
            this.lblMessage.Text = "Ready ...";
            // 
            // txtFileContents
            // 
            this.txtFileContents.AcceptsReturn = true;
            this.txtFileContents.AcceptsTab = true;
            this.txtFileContents.Location = new System.Drawing.Point(191, 196);
            this.txtFileContents.MaxLength = 2000000;
            this.txtFileContents.Multiline = true;
            this.txtFileContents.Name = "txtFileContents";
            this.txtFileContents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFileContents.Size = new System.Drawing.Size(499, 134);
            this.txtFileContents.TabIndex = 2;
            // 
            // VendorList
            // 
            this.VendorList.CheckOnClick = true;
            this.VendorList.FormattingEnabled = true;
            this.VendorList.Location = new System.Drawing.Point(696, 80);
            this.VendorList.Name = "VendorList";
            this.VendorList.Size = new System.Drawing.Size(257, 304);
            this.VendorList.TabIndex = 0;
            this.VendorList.Visible = false;
            this.VendorList.SelectedIndexChanged += new System.EventHandler(this.VendorList_SelectedIndexChanged);
            // 
            // lblEmails
            // 
            this.lblEmails.AutoSize = true;
            this.lblEmails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmails.Location = new System.Drawing.Point(12, 39);
            this.lblEmails.Name = "lblEmails";
            this.lblEmails.Size = new System.Drawing.Size(149, 17);
            this.lblEmails.TabIndex = 6;
            this.lblEmails.Text = "EMAIL TEMPLATES";
            // 
            // btnExportBlog
            // 
            this.btnExportBlog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportBlog.Location = new System.Drawing.Point(696, 415);
            this.btnExportBlog.Name = "btnExportBlog";
            this.btnExportBlog.Size = new System.Drawing.Size(118, 30);
            this.btnExportBlog.TabIndex = 8;
            this.btnExportBlog.Text = "Export HTML /Blog";
            this.btnExportBlog.UseVisualStyleBackColor = true;
            this.btnExportBlog.Visible = false;
            this.btnExportBlog.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtTemplateName
            // 
            this.txtTemplateName.Location = new System.Drawing.Point(191, 56);
            this.txtTemplateName.Name = "txtTemplateName";
            this.txtTemplateName.Size = new System.Drawing.Size(499, 20);
            this.txtTemplateName.TabIndex = 9;
            // 
            // lstBlogs
            // 
            this.lstBlogs.FormattingEnabled = true;
            this.lstBlogs.Location = new System.Drawing.Point(1268, 641);
            this.lstBlogs.Name = "lstBlogs";
            this.lstBlogs.Size = new System.Drawing.Size(151, 69);
            this.lstBlogs.TabIndex = 10;
            this.lstBlogs.SelectedIndexChanged += new System.EventHandler(this.lstBlogs_SelectedIndexChanged);
            this.lstBlogs.SelectedValueChanged += new System.EventHandler(this.lstBlogs_SelectedValueChanged);
            // 
            // lstEmails
            // 
            this.lstEmails.FormattingEnabled = true;
            this.lstEmails.Location = new System.Drawing.Point(12, 58);
            this.lstEmails.Name = "lstEmails";
            this.lstEmails.Size = new System.Drawing.Size(163, 654);
            this.lstEmails.TabIndex = 11;
            this.lstEmails.SelectedValueChanged += new System.EventHandler(this.lstEmails_SelectedValueChanged);
            // 
            // lblVendors
            // 
            this.lblVendors.AutoSize = true;
            this.lblVendors.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendors.Location = new System.Drawing.Point(694, 33);
            this.lblVendors.Name = "lblVendors";
            this.lblVendors.Size = new System.Drawing.Size(68, 17);
            this.lblVendors.TabIndex = 12;
            this.lblVendors.Text = "Vendors";
            this.lblVendors.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(188, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Output Directory";
            // 
            // txtOutputDirectory
            // 
            this.txtOutputDirectory.Enabled = false;
            this.txtOutputDirectory.Location = new System.Drawing.Point(276, 2);
            this.txtOutputDirectory.Name = "txtOutputDirectory";
            this.txtOutputDirectory.Size = new System.Drawing.Size(439, 20);
            this.txtOutputDirectory.TabIndex = 14;
            // 
            // btnDirSearch
            // 
            this.btnDirSearch.Location = new System.Drawing.Point(722, 1);
            this.btnDirSearch.Name = "btnDirSearch";
            this.btnDirSearch.Size = new System.Drawing.Size(30, 22);
            this.btnDirSearch.TabIndex = 15;
            this.btnDirSearch.Text = "...";
            this.btnDirSearch.UseVisualStyleBackColor = true;
            this.btnDirSearch.Click += new System.EventHandler(this.btnDirSearch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(267, 413);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(151, 36);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnOutput
            // 
            this.btnOutput.Location = new System.Drawing.Point(757, 1);
            this.btnOutput.Name = "btnOutput";
            this.btnOutput.Size = new System.Drawing.Size(120, 22);
            this.btnOutput.TabIndex = 17;
            this.btnOutput.Text = "Show Output";
            this.btnOutput.UseVisualStyleBackColor = true;
            this.btnOutput.Visible = false;
            this.btnOutput.Click += new System.EventHandler(this.btnOutput_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(423, 413);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(134, 36);
            this.btnUpdate.TabIndex = 18;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtVendorNameFilter
            // 
            this.txtVendorNameFilter.Location = new System.Drawing.Point(696, 56);
            this.txtVendorNameFilter.Name = "txtVendorNameFilter";
            this.txtVendorNameFilter.Size = new System.Drawing.Size(145, 20);
            this.txtVendorNameFilter.TabIndex = 19;
            this.txtVendorNameFilter.Visible = false;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(847, 54);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(89, 23);
            this.btnFilter.TabIndex = 20;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Visible = false;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // btnSaveBlog
            // 
            this.btnSaveBlog.Location = new System.Drawing.Point(696, 465);
            this.btnSaveBlog.Name = "btnSaveBlog";
            this.btnSaveBlog.Size = new System.Drawing.Size(118, 30);
            this.btnSaveBlog.TabIndex = 21;
            this.btnSaveBlog.Text = "Save Blog";
            this.btnSaveBlog.UseVisualStyleBackColor = true;
            this.btnSaveBlog.Visible = false;
            this.btnSaveBlog.Click += new System.EventHandler(this.btnSaveBlog_Click);
            // 
            // btnSaveEmail
            // 
            this.btnSaveEmail.Location = new System.Drawing.Point(815, 465);
            this.btnSaveEmail.Name = "btnSaveEmail";
            this.btnSaveEmail.Size = new System.Drawing.Size(142, 30);
            this.btnSaveEmail.TabIndex = 22;
            this.btnSaveEmail.Text = "Save Email";
            this.btnSaveEmail.UseVisualStyleBackColor = true;
            this.btnSaveEmail.Visible = false;
            this.btnSaveEmail.Click += new System.EventHandler(this.btnSaveEmail_Click);
            // 
            // btnExportEmail
            // 
            this.btnExportEmail.Location = new System.Drawing.Point(815, 415);
            this.btnExportEmail.Name = "btnExportEmail";
            this.btnExportEmail.Size = new System.Drawing.Size(142, 30);
            this.btnExportEmail.TabIndex = 23;
            this.btnExportEmail.Text = "Export MC Email Template";
            this.btnExportEmail.UseVisualStyleBackColor = true;
            this.btnExportEmail.Visible = false;
            this.btnExportEmail.Click += new System.EventHandler(this.btnExportEmail_Click);
            // 
            // chkCheckAll
            // 
            this.chkCheckAll.AutoSize = true;
            this.chkCheckAll.Location = new System.Drawing.Point(770, 36);
            this.chkCheckAll.Name = "chkCheckAll";
            this.chkCheckAll.Size = new System.Drawing.Size(71, 17);
            this.chkCheckAll.TabIndex = 24;
            this.chkCheckAll.Text = "Check All";
            this.chkCheckAll.UseVisualStyleBackColor = true;
            this.chkCheckAll.Visible = false;
            this.chkCheckAll.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // CourseList
            // 
            this.CourseList.CheckOnClick = true;
            this.CourseList.FormattingEnabled = true;
            this.CourseList.Location = new System.Drawing.Point(962, 438);
            this.CourseList.Name = "CourseList";
            this.CourseList.Size = new System.Drawing.Size(300, 274);
            this.CourseList.TabIndex = 25;
            this.CourseList.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(696, 501);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(261, 15);
            this.progressBar1.TabIndex = 28;
            this.progressBar1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(959, 424);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Select additional courses";
            this.label1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(955, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Top Pick";
            this.label3.Visible = false;
            // 
            // lstTopPickCourse
            // 
            this.lstTopPickCourse.FormattingEnabled = true;
            this.lstTopPickCourse.Location = new System.Drawing.Point(959, 78);
            this.lstTopPickCourse.Name = "lstTopPickCourse";
            this.lstTopPickCourse.Size = new System.Drawing.Size(303, 329);
            this.lstTopPickCourse.TabIndex = 31;
            this.lstTopPickCourse.Visible = false;
            this.lstTopPickCourse.Click += new System.EventHandler(this.lstTopPickCourse_Click);
            this.lstTopPickCourse.SelectedIndexChanged += new System.EventHandler(this.lstTopPickCourse_SelectedIndexChanged);
            // 
            // btnEmailBlast
            // 
            this.btnEmailBlast.BackColor = System.Drawing.Color.Goldenrod;
            this.btnEmailBlast.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmailBlast.Location = new System.Drawing.Point(423, 488);
            this.btnEmailBlast.Name = "btnEmailBlast";
            this.btnEmailBlast.Size = new System.Drawing.Size(134, 33);
            this.btnEmailBlast.TabIndex = 32;
            this.btnEmailBlast.Text = "Send Email Blast";
            this.btnEmailBlast.UseVisualStyleBackColor = false;
            this.btnEmailBlast.Visible = false;
            this.btnEmailBlast.Click += new System.EventHandler(this.btnEmailBlast_Click);
            // 
            // lstMailChimpTemplates
            // 
            this.lstMailChimpTemplates.FormattingEnabled = true;
            this.lstMailChimpTemplates.Location = new System.Drawing.Point(187, 539);
            this.lstMailChimpTemplates.Name = "lstMailChimpTemplates";
            this.lstMailChimpTemplates.Size = new System.Drawing.Size(279, 173);
            this.lstMailChimpTemplates.TabIndex = 33;
            this.lstMailChimpTemplates.SelectedIndexChanged += new System.EventHandler(this.lstMailChimpTemplates_SelectedIndexChanged);
            // 
            // lstMailChimpList
            // 
            this.lstMailChimpList.FormattingEnabled = true;
            this.lstMailChimpList.Location = new System.Drawing.Point(472, 539);
            this.lstMailChimpList.Name = "lstMailChimpList";
            this.lstMailChimpList.Size = new System.Drawing.Size(280, 173);
            this.lstMailChimpList.TabIndex = 34;
            this.lstMailChimpList.SelectedIndexChanged += new System.EventHandler(this.lstMailChimpList_SelectedIndexChanged);
            // 
            // lstMailChimpCampaigns
            // 
            this.lstMailChimpCampaigns.FormattingEnabled = true;
            this.lstMailChimpCampaigns.Location = new System.Drawing.Point(758, 539);
            this.lstMailChimpCampaigns.Name = "lstMailChimpCampaigns";
            this.lstMailChimpCampaigns.Size = new System.Drawing.Size(199, 173);
            this.lstMailChimpCampaigns.TabIndex = 35;
            this.lstMailChimpCampaigns.SelectedIndexChanged += new System.EventHandler(this.lstMailChimpCampaigns_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(184, 525);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "MailChimp Templates";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(469, 523);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "MailChimp Email Group";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(755, 523);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "MailChimp Campaigns";
            // 
            // btnCreateCampaign
            // 
            this.btnCreateCampaign.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnCreateCampaign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateCampaign.Location = new System.Drawing.Point(423, 451);
            this.btnCreateCampaign.Name = "btnCreateCampaign";
            this.btnCreateCampaign.Size = new System.Drawing.Size(134, 33);
            this.btnCreateCampaign.TabIndex = 39;
            this.btnCreateCampaign.Text = "Create Campaign";
            this.btnCreateCampaign.UseVisualStyleBackColor = false;
            this.btnCreateCampaign.Visible = false;
            this.btnCreateCampaign.Click += new System.EventHandler(this.btnCreateCampaign_Click);
            // 
            // btnDeleteCampaign
            // 
            this.btnDeleteCampaign.BackColor = System.Drawing.Color.OrangeRed;
            this.btnDeleteCampaign.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteCampaign.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDeleteCampaign.Location = new System.Drawing.Point(560, 487);
            this.btnDeleteCampaign.Name = "btnDeleteCampaign";
            this.btnDeleteCampaign.Size = new System.Drawing.Size(129, 33);
            this.btnDeleteCampaign.TabIndex = 40;
            this.btnDeleteCampaign.Text = "Delete Campaign";
            this.btnDeleteCampaign.UseVisualStyleBackColor = false;
            this.btnDeleteCampaign.Click += new System.EventHandler(this.btnDeleteCampaign_Click);
            // 
            // btnDeleteTemplate
            // 
            this.btnDeleteTemplate.BackColor = System.Drawing.Color.Tomato;
            this.btnDeleteTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteTemplate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnDeleteTemplate.Location = new System.Drawing.Point(560, 451);
            this.btnDeleteTemplate.Name = "btnDeleteTemplate";
            this.btnDeleteTemplate.Size = new System.Drawing.Size(129, 33);
            this.btnDeleteTemplate.TabIndex = 41;
            this.btnDeleteTemplate.Text = "Delete Template";
            this.btnDeleteTemplate.UseVisualStyleBackColor = false;
            this.btnDeleteTemplate.Click += new System.EventHandler(this.btnDeleteTemplate_Click);
            // 
            // txtAddComments
            // 
            this.txtAddComments.Location = new System.Drawing.Point(191, 140);
            this.txtAddComments.Multiline = true;
            this.txtAddComments.Name = "txtAddComments";
            this.txtAddComments.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtAddComments.Size = new System.Drawing.Size(498, 49);
            this.txtAddComments.TabIndex = 42;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.SteelBlue;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAdd.Location = new System.Drawing.Point(560, 413);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(130, 36);
            this.btnAdd.TabIndex = 43;
            this.btnAdd.Text = "Add New";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtPromotional
            // 
            this.txtPromotional.Location = new System.Drawing.Point(191, 337);
            this.txtPromotional.Multiline = true;
            this.txtPromotional.Name = "txtPromotional";
            this.txtPromotional.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPromotional.Size = new System.Drawing.Size(499, 47);
            this.txtPromotional.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(594, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Template Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(559, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "Template Contents";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(633, 101);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Tag line";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(583, 332);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 13);
            this.label10.TabIndex = 48;
            this.label10.Text = "Promotional Offer";
            // 
            // txtExportName
            // 
            this.txtExportName.Location = new System.Drawing.Point(1142, 29);
            this.txtExportName.Name = "txtExportName";
            this.txtExportName.Size = new System.Drawing.Size(119, 20);
            this.txtExportName.TabIndex = 49;
            this.txtExportName.Visible = false;
            // 
            // txtTagLine
            // 
            this.txtTagLine.Location = new System.Drawing.Point(191, 110);
            this.txtTagLine.Name = "txtTagLine";
            this.txtTagLine.Size = new System.Drawing.Size(499, 20);
            this.txtTagLine.TabIndex = 50;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(619, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "Description";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(1023, 54);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(238, 20);
            this.txtSearch.TabIndex = 52;
            this.txtSearch.Visible = false;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(189, 395);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 53;
            this.label12.Text = "Promo Code";
            // 
            // txtPromoCode
            // 
            this.txtPromoCode.Location = new System.Drawing.Point(269, 390);
            this.txtPromoCode.Name = "txtPromoCode";
            this.txtPromoCode.Size = new System.Drawing.Size(68, 20);
            this.txtPromoCode.TabIndex = 54;
            // 
            // txtPromoAddlComnt
            // 
            this.txtPromoAddlComnt.Location = new System.Drawing.Point(374, 390);
            this.txtPromoAddlComnt.Name = "txtPromoAddlComnt";
            this.txtPromoAddlComnt.Size = new System.Drawing.Size(206, 20);
            this.txtPromoAddlComnt.TabIndex = 55;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(409, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 56;
            this.label13.Text = "MC API Key";
            // 
            // ddMailChimpAPIKey
            // 
            this.ddMailChimpAPIKey.FormattingEnabled = true;
            this.ddMailChimpAPIKey.Location = new System.Drawing.Point(489, 29);
            this.ddMailChimpAPIKey.Name = "ddMailChimpAPIKey";
            this.ddMailChimpAPIKey.Size = new System.Drawing.Size(201, 21);
            this.ddMailChimpAPIKey.TabIndex = 57;
            this.ddMailChimpAPIKey.SelectedIndexChanged += new System.EventHandler(this.ddMailChimpAPIKey_SelectedIndexChanged);
            // 
            // txtMCFrom
            // 
            this.txtMCFrom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMCFrom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtMCFrom.Location = new System.Drawing.Point(267, 476);
            this.txtMCFrom.Name = "txtMCFrom";
            this.txtMCFrom.Size = new System.Drawing.Size(151, 20);
            this.txtMCFrom.TabIndex = 58;
            // 
            // txtMCSubject
            // 
            this.txtMCSubject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMCSubject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtMCSubject.Location = new System.Drawing.Point(267, 501);
            this.txtMCSubject.Name = "txtMCSubject";
            this.txtMCSubject.Size = new System.Drawing.Size(151, 20);
            this.txtMCSubject.TabIndex = 59;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(186, 480);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 60;
            this.label14.Text = "MC From";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(186, 503);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 61;
            this.label15.Text = "MC Subject";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(186, 453);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 13);
            this.label16.TabIndex = 62;
            this.label16.Text = "MC Email";
            // 
            // txtMCEmail
            // 
            this.txtMCEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMCEmail.ForeColor = System.Drawing.Color.Maroon;
            this.txtMCEmail.Location = new System.Drawing.Point(267, 451);
            this.txtMCEmail.Name = "txtMCEmail";
            this.txtMCEmail.Size = new System.Drawing.Size(151, 20);
            this.txtMCEmail.TabIndex = 63;
            // 
            // txtLogoLine
            // 
            this.txtLogoLine.Location = new System.Drawing.Point(191, 83);
            this.txtLogoLine.Name = "txtLogoLine";
            this.txtLogoLine.Size = new System.Drawing.Size(498, 20);
            this.txtLogoLine.TabIndex = 64;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(651, 80);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 65;
            this.label17.Text = "Logo";
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(1142, 409);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(120, 27);
            this.btnPreview.TabIndex = 66;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Visible = false;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // chkIncludeFBTwitter
            // 
            this.chkIncludeFBTwitter.AutoSize = true;
            this.chkIncludeFBTwitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIncludeFBTwitter.Location = new System.Drawing.Point(816, 448);
            this.chkIncludeFBTwitter.Name = "chkIncludeFBTwitter";
            this.chkIncludeFBTwitter.Size = new System.Drawing.Size(130, 17);
            this.chkIncludeFBTwitter.TabIndex = 67;
            this.chkIncludeFBTwitter.Text = "Include fb - twitter";
            this.chkIncludeFBTwitter.UseVisualStyleBackColor = true;
            this.chkIncludeFBTwitter.Visible = false;
            // 
            // btnAddPromoCode
            // 
            this.btnAddPromoCode.BackColor = System.Drawing.Color.DarkGreen;
            this.btnAddPromoCode.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddPromoCode.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddPromoCode.Location = new System.Drawing.Point(856, 388);
            this.btnAddPromoCode.Name = "btnAddPromoCode";
            this.btnAddPromoCode.Size = new System.Drawing.Size(97, 23);
            this.btnAddPromoCode.TabIndex = 68;
            this.btnAddPromoCode.Text = "Add Promo Code";
            this.btnAddPromoCode.UseVisualStyleBackColor = false;
            this.btnAddPromoCode.Visible = false;
            this.btnAddPromoCode.Click += new System.EventHandler(this.btnAddPromoCode_Click);
            // 
            // txtDiscountAmt
            // 
            this.txtDiscountAmt.Location = new System.Drawing.Point(341, 390);
            this.txtDiscountAmt.MaxLength = 3;
            this.txtDiscountAmt.Name = "txtDiscountAmt";
            this.txtDiscountAmt.Size = new System.Drawing.Size(30, 20);
            this.txtDiscountAmt.TabIndex = 69;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(505, 384);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 70;
            this.label18.Text = "Promo add\'l cmt";
            // 
            // rd1
            // 
            this.rd1.AutoSize = true;
            this.rd1.Location = new System.Drawing.Point(14, 18);
            this.rd1.Name = "rd1";
            this.rd1.Size = new System.Drawing.Size(53, 17);
            this.rd1.TabIndex = 72;
            this.rd1.TabStop = true;
            this.rd1.Text = "Name";
            this.rd1.UseVisualStyleBackColor = true;
            this.rd1.CheckedChanged += new System.EventHandler(this.rd1_CheckedChanged);
            // 
            // rd2
            // 
            this.rd2.AutoSize = true;
            this.rd2.Location = new System.Drawing.Point(82, 18);
            this.rd2.Name = "rd2";
            this.rd2.Size = new System.Drawing.Size(70, 17);
            this.rd2.TabIndex = 73;
            this.rd2.TabStop = true;
            this.rd2.Text = "Course Id";
            this.rd2.UseVisualStyleBackColor = true;
            this.rd2.CheckedChanged += new System.EventHandler(this.rd2_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rd1);
            this.groupBox1.Controls.Add(this.rd2);
            this.groupBox1.Location = new System.Drawing.Point(950, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(158, 41);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sort By";
            // 
            // txtPromoExpDate
            // 
            this.txtPromoExpDate.Location = new System.Drawing.Point(586, 390);
            this.txtPromoExpDate.Name = "txtPromoExpDate";
            this.txtPromoExpDate.Size = new System.Drawing.Size(104, 20);
            this.txtPromoExpDate.TabIndex = 75;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(648, 384);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 76;
            this.label19.Text = "Exp.Date";
            // 
            // txtPromoDBComment
            // 
            this.txtPromoDBComment.Location = new System.Drawing.Point(696, 390);
            this.txtPromoDBComment.Name = "txtPromoDBComment";
            this.txtPromoDBComment.Size = new System.Drawing.Size(154, 20);
            this.txtPromoDBComment.TabIndex = 77;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(754, 384);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(102, 13);
            this.label20.TabIndex = 78;
            this.label20.Text = "Promo DB Comment";
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1268, 722);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtPromoDBComment);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtPromoExpDate);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtDiscountAmt);
            this.Controls.Add(this.btnAddPromoCode);
            this.Controls.Add(this.chkIncludeFBTwitter);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtLogoLine);
            this.Controls.Add(this.txtMCEmail);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtMCSubject);
            this.Controls.Add(this.txtMCFrom);
            this.Controls.Add(this.ddMailChimpAPIKey);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPromoAddlComnt);
            this.Controls.Add(this.txtPromoCode);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtExportName);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtPromotional);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtAddComments);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSaveBlog);
            this.Controls.Add(this.lstBlogs);
            this.Controls.Add(this.btnDeleteTemplate);
            this.Controls.Add(this.btnCreateCampaign);
            this.Controls.Add(this.btnSaveEmail);
            this.Controls.Add(this.chkCheckAll);
            this.Controls.Add(this.VendorList);
            this.Controls.Add(this.lblEmails);
            this.Controls.Add(this.btnDeleteCampaign);
            this.Controls.Add(this.btnOutput);
            this.Controls.Add(this.btnExportBlog);
            this.Controls.Add(this.lstEmails);
            this.Controls.Add(this.btnEmailBlast);
            this.Controls.Add(this.btnDirSearch);
            this.Controls.Add(this.txtFileContents);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtOutputDirectory);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstMailChimpTemplates);
            this.Controls.Add(this.lblVendors);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.txtTemplateName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstMailChimpList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnExportEmail);
            this.Controls.Add(this.lstTopPickCourse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.lstMailChimpCampaigns);
            this.Controls.Add(this.CourseList);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.txtVendorNameFilter);
            this.Controls.Add(this.txtTagLine);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "ZipEdTech Blog Management";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txtFileContents;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.CheckedListBox VendorList;
        private System.Windows.Forms.ToolStripMenuItem blogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailToolStripMenuItem;
        private System.Windows.Forms.Label lblEmails;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toBlogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toEmailBlastToolStripMenuItem;
        private System.Windows.Forms.Button btnExportBlog;
        private System.Windows.Forms.TextBox txtTemplateName;
        private System.Windows.Forms.ListBox lstBlogs;
        private System.Windows.Forms.ListBox lstEmails;
        private System.Windows.Forms.Label lblVendors;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOutputDirectory;
        private System.Windows.Forms.Button btnDirSearch;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnOutput;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtVendorNameFilter;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutThisSoftwareToolStripMenuItem;
        private System.Windows.Forms.Button btnSaveBlog;
        private System.Windows.Forms.Button btnSaveEmail;
        private System.Windows.Forms.Button btnExportEmail;
        private System.Windows.Forms.CheckBox chkCheckAll;
        private System.Windows.Forms.CheckedListBox CourseList;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstTopPickCourse;
        private System.Windows.Forms.Button btnEmailBlast;
        private System.Windows.Forms.ListBox lstMailChimpTemplates;
        private System.Windows.Forms.ListBox lstMailChimpList;
        private System.Windows.Forms.ListBox lstMailChimpCampaigns;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCreateCampaign;
        private System.Windows.Forms.Button btnDeleteCampaign;
        private System.Windows.Forms.Button btnDeleteTemplate;
        private System.Windows.Forms.TextBox txtAddComments;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtPromotional;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtExportName;
        private System.Windows.Forms.TextBox txtTagLine;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPromoCode;
        private System.Windows.Forms.TextBox txtPromoAddlComnt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ddMailChimpAPIKey;
        private System.Windows.Forms.TextBox txtMCFrom;
        private System.Windows.Forms.TextBox txtMCSubject;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtMCEmail;
        private System.Windows.Forms.TextBox txtLogoLine;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.CheckBox chkIncludeFBTwitter;
        private System.Windows.Forms.Button btnAddPromoCode;
        private System.Windows.Forms.TextBox txtDiscountAmt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RadioButton rd1;
        private System.Windows.Forms.RadioButton rd2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPromoExpDate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtPromoDBComment;
        private System.Windows.Forms.Label label20;
    }
}

