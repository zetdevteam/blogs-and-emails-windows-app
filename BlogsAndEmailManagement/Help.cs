﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;

namespace BlogsAndEmailManagement
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                Blogs aboutSoftware = zd.Blogs.SingleOrDefault(x => x.Type == 99);
                if (aboutSoftware != null)
                    this.txtAboutSoftware.Text = aboutSoftware.Contents;

                
            }
        }
    }
}
