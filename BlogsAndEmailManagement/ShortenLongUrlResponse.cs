﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogsAndEmailManagement
{
    public class ShortenLongUrlResponse
    {
        public string kind { get; set; }
        public string id { get; set; }
        public string longUrl { get; set; }
    }
}
