﻿using System;
using System.Linq;
using DAL;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using MailChimp;
using System.Collections.Generic;

namespace BlogsAndEmailManagement
{
    public static class StaticClass
    {
        public static byte TemplateType {get;set;}

        public static string GetShortURL(string longUrl)
        {
            UriBuilder uriBuilder = new UriBuilder("https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAM6IcTfEDIhfrGXQFC3htwtfNSnWOnGBM");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriBuilder.Uri);
            request.Method = "POST";
            request.ContentType = "application/json";

            var postBody = string.Format(@"{{""longUrl"": ""{0}""}}", longUrl);
            var postData = Encoding.ASCII.GetBytes(postBody);
            request.ContentLength = postData.Length;
            var reqStream = request.GetRequestStream();

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(postData, 0, postData.Length);
                postStream.Close();
            }
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var respStream = new StreamReader(response.GetResponseStream()))
                {
                    var responseBody = respStream.ReadToEnd();
                    var deserializer = new JavaScriptSerializer();
                    var results = deserializer.Deserialize<ShortenLongUrlResponse>(responseBody);
                    return results.id;
                }
            }
        }

        public static string StoreTemplate(string templateName, string contents, byte templateType, string logoLine, string tagline, string comments, string promo, string promoCode, string promo2)  // Template Type 1: Blogs, 2: Email Blast
        {
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                zd.Blogs.Add(new Blogs() { Contents = contents, Name = templateName, Type = templateType, RCD = DateTime.Now, LogoLine = logoLine,  TagLine = tagline, Comments = comments, PromotionalComments = promo, PromoCode = promoCode, PromoCodeComment2 = promo2 });
                if (zd.SaveChanges() > 0)
                    return "Record has been added";
                else
                    return "Record not added";
            }
        }

        public static string CreateTemplate(string templateName, string contents, string logoLine, string tagline, string comments, byte type, string promo, string promoCode, string promo2)  
        {
            if (string.IsNullOrEmpty(templateName.Trim()) || string.IsNullOrEmpty(contents.Trim()))
            {
                return "Please type the template name and contents";
            }

            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                Blogs existingBlog = zd.Blogs.SingleOrDefault(x => x.Name == templateName);
                if (existingBlog != null)
                {
                    return "Please choose a different name";
                }
               
                zd.Blogs.Add(new Blogs() { Name = templateName, Contents = contents, LogoLine = logoLine, TagLine = tagline, Comments = comments, PromotionalComments = promo, PromoCode = promoCode, PromoCodeComment2 = promo2, RCD = DateTime.Now, Type = type});
                if (zd.SaveChanges() > 0)
                    return "Record has been added";
                else
                    return "Record not added";
            }
        }

        public static string UpdateTemplate(long blogId, string templateName, string contents, string logoLine, string tagline, string comments, string promo, string promoCode, string promo2)  // Template Type 1: Blogs, 2: Email Blast
        {
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                Blogs existingBlog = zd.Blogs.Single(x => x.BlogId == blogId);
                existingBlog.Contents = contents;
                existingBlog.Name = templateName;
                existingBlog.Comments = comments;
                existingBlog.PromotionalComments = promo;
                existingBlog.PromoCode = promoCode;
                existingBlog.PromoCodeComment2 = promo2;
                existingBlog.TagLine = tagline;
                existingBlog.LogoLine = logoLine;
                if (zd.SaveChanges() > 0)
                    return "Record has been updated";
                else
                    return "Record not updated";
            }
        }

        public static void UpdateMailChimpRecord(int templateId, string fromEmail, string fromName, string Subject)
        {
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                MailChimpTemplate mt = zd.MailChimpTemplate.SingleOrDefault(x => x.MailChimpTemplateId == templateId);
                if (mt != null)
                {
                    mt.FromName = fromName;
                    mt.EmailAddressFrom = fromEmail;
                    mt.SubjectLine = Subject;
                    zd.SaveChanges();
                }
            }
        }

        public static void DeleteMailChimpRecord(int templateId)
        {
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                MailChimpTemplate mt = zd.MailChimpTemplate.SingleOrDefault(x => x.MailChimpTemplateId == templateId);
                if (mt != null)
                {
                    zd.MailChimpTemplate.Remove(mt);
                    zd.SaveChanges();
                }
            }
        }

        public static string DeleteTemplate(long blogId) 
        {
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                Blogs existingBlog = zd.Blogs.Single(x => x.BlogId == blogId);
                zd.Blogs.Remove(existingBlog);
                if (zd.SaveChanges() > 0)
                    return "Record has been deleted";
                else
                    return "Record not deleted";
            }
        }

        public static Blogs GetTemplate(long blogId)
        {
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                Blogs existingBlog = zd.Blogs.Single(x => x.BlogId == blogId);
                TemplateType = existingBlog.Type;
                return existingBlog;
            }
        }

        public static Blogs[] GetTemplates(byte? type = null)
        {
            using (ZET_DATAEntities zd = new ZET_DATAEntities())
            {
                if (type != null)
                    return zd.Blogs.Where(x => x.Type == type).OrderBy(x => x.Name).ToArray();
                else
                    return zd.Blogs.ToArray();
            }
        }

        public static string ParseDirectory(string filepath, byte whichSegment)
        {
            string[] segments = filepath.Split('\\');
            if (segments.Length < 2)
                return filepath;

            if (whichSegment == 1)
            {
                return filepath.Replace(segments[segments.Length - 1], "");
            }
            else
            {
                string[] filenames = segments[segments.Length - 1].Split('.');
                return filenames[0];
            }
        }

    }
}
