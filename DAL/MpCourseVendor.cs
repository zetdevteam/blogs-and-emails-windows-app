//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MpCourseVendor
    {
        public int VendorId { get; set; }
        public int CourseId { get; set; }
        public Nullable<double> Discount { get; set; }
        public Nullable<int> DisplaySequence { get; set; }
    
        public virtual MpCourse MpCourse { get; set; }
        public virtual MpVendor MpVendor { get; set; }
    }
}
